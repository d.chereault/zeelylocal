<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210506203124 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE artiste');
        $this->addSql('DROP TABLE image');
        $this->addSql('DROP TABLE users');
        $this->addSql('DROP INDEX idArtiste ON concert');
        $this->addSql('ALTER TABLE concert ADD nb_places INT NOT NULL, ADD date_concert DATETIME NOT NULL, DROP idArtiste, DROP dateconcert, CHANGE image image VARCHAR(200) DEFAULT NULL, CHANGE prix prix DOUBLE PRECISION DEFAULT NULL, CHANGE nbplaces id_artiste INT NOT NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE artiste (idArtiste INT UNSIGNED AUTO_INCREMENT NOT NULL, Nom VARCHAR(50) CHARACTER SET latin1 DEFAULT NULL COLLATE `latin1_swedish_ci`, Prenom VARCHAR(50) CHARACTER SET latin1 DEFAULT NULL COLLATE `latin1_swedish_ci`, NomArtiste VARCHAR(50) CHARACTER SET latin1 NOT NULL COLLATE `latin1_swedish_ci`, date_naissance DATETIME DEFAULT NULL, INDEX NomArtiste (NomArtiste), PRIMARY KEY(idArtiste)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('CREATE TABLE image (IdImage SMALLINT AUTO_INCREMENT NOT NULL, source VARCHAR(500) CHARACTER SET latin1 DEFAULT NULL COLLATE `latin1_swedish_ci`, libelle VARCHAR(50) CHARACTER SET latin1 DEFAULT NULL COLLATE `latin1_swedish_ci`, PRIMARY KEY(IdImage)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('CREATE TABLE users (IdUsers INT UNSIGNED AUTO_INCREMENT NOT NULL, FirstName VARCHAR(50) CHARACTER SET latin1 NOT NULL COLLATE `latin1_swedish_ci`, LastName VARCHAR(50) CHARACTER SET latin1 NOT NULL COLLATE `latin1_swedish_ci`, Username VARCHAR(50) CHARACTER SET latin1 NOT NULL COLLATE `latin1_swedish_ci`, Password VARCHAR(42) CHARACTER SET latin1 NOT NULL COLLATE `latin1_swedish_ci`, Adresse VARCHAR(200) CHARACTER SET latin1 DEFAULT NULL COLLATE `latin1_swedish_ci`, email VARCHAR(150) CHARACTER SET latin1 NOT NULL COLLATE `latin1_swedish_ci`, DDN DATE NOT NULL, PRIMARY KEY(IdUsers)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('ALTER TABLE concert ADD idArtiste INT UNSIGNED NOT NULL, ADD nbplaces INT NOT NULL, ADD dateconcert DATE DEFAULT NULL, DROP id_artiste, DROP nb_places, DROP date_concert, CHANGE image image VARCHAR(100) CHARACTER SET latin1 DEFAULT NULL COLLATE `latin1_swedish_ci`, CHANGE prix prix DOUBLE PRECISION UNSIGNED DEFAULT NULL');
        $this->addSql('CREATE INDEX idArtiste ON concert (idArtiste)');
    }
}
