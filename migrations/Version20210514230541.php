<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210514230541 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP INDEX NomArtiste ON artiste');
        $this->addSql('ALTER TABLE artiste CHANGE id id INT AUTO_INCREMENT NOT NULL');
        $this->addSql('DROP INDEX idArtiste ON concert');
        $this->addSql('ALTER TABLE concert CHANGE id_artiste id_artiste INT NOT NULL, CHANGE date_concert date_concert DATETIME NOT NULL, CHANGE image image VARCHAR(200) DEFAULT NULL, CHANGE prix prix DOUBLE PRECISION DEFAULT NULL');
        $this->addSql('ALTER TABLE users CHANGE id id INT AUTO_INCREMENT NOT NULL, CHANGE ddn ddn DATETIME NOT NULL, CHANGE roles roles VARCHAR(20) NOT NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE artiste CHANGE id id INT UNSIGNED AUTO_INCREMENT NOT NULL');
        $this->addSql('CREATE INDEX NomArtiste ON artiste (nom_artiste)');
        $this->addSql('ALTER TABLE concert CHANGE id_artiste id_artiste INT UNSIGNED NOT NULL, CHANGE date_concert date_concert DATE DEFAULT NULL, CHANGE image image VARCHAR(100) CHARACTER SET latin1 DEFAULT NULL COLLATE `latin1_swedish_ci`, CHANGE prix prix DOUBLE PRECISION UNSIGNED DEFAULT NULL');
        $this->addSql('CREATE INDEX idArtiste ON concert (id_artiste)');
        $this->addSql('ALTER TABLE users CHANGE id id INT UNSIGNED AUTO_INCREMENT NOT NULL, CHANGE ddn ddn DATE NOT NULL, CHANGE roles roles VARCHAR(20) CHARACTER SET latin1 DEFAULT \'USER\' NOT NULL COLLATE `latin1_swedish_ci`');
    }
}
