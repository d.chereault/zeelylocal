<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210507085552 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE image');
        $this->addSql('DROP INDEX NomArtiste ON artiste');
        $this->addSql('ALTER TABLE artiste CHANGE id id INT AUTO_INCREMENT NOT NULL');
        $this->addSql('DROP INDEX idArtiste ON concert');
        $this->addSql('ALTER TABLE concert CHANGE id_artiste id_artiste INT NOT NULL, CHANGE date_concert date_concert DATETIME NOT NULL, CHANGE image image VARCHAR(200) DEFAULT NULL, CHANGE prix prix DOUBLE PRECISION DEFAULT NULL');
        $this->addSql('ALTER TABLE users CHANGE id id INT AUTO_INCREMENT NOT NULL, CHANGE ddn ddn DATETIME NOT NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE image (Id SMALLINT AUTO_INCREMENT NOT NULL, source VARCHAR(500) CHARACTER SET latin1 DEFAULT NULL COLLATE `latin1_swedish_ci`, libelle VARCHAR(50) CHARACTER SET latin1 DEFAULT NULL COLLATE `latin1_swedish_ci`, PRIMARY KEY(Id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('ALTER TABLE artiste CHANGE id id INT UNSIGNED AUTO_INCREMENT NOT NULL');
        $this->addSql('CREATE INDEX NomArtiste ON artiste (nom_artiste)');
        $this->addSql('ALTER TABLE concert CHANGE id_artiste id_artiste INT UNSIGNED NOT NULL, CHANGE date_concert date_concert DATE DEFAULT NULL, CHANGE image image VARCHAR(100) CHARACTER SET latin1 DEFAULT NULL COLLATE `latin1_swedish_ci`, CHANGE prix prix DOUBLE PRECISION UNSIGNED DEFAULT NULL');
        $this->addSql('CREATE INDEX idArtiste ON concert (id_artiste)');
        $this->addSql('ALTER TABLE users CHANGE id id INT UNSIGNED AUTO_INCREMENT NOT NULL, CHANGE ddn ddn DATE NOT NULL');
    }
}
